titlescreen:
		move.w #$0018, height
		move.w #$0030, streamers
		move.w #$0003, speed
		move.l #$60000000,(a3)
		lea (ascii),a5
		move.w #$0BFF,d4
		bsr vram_loop
		lea (title),a5
		move.l #$60000002,(a3)
		bsr termtextloop
		move.w #$ab4a,cursorpos
titleloop:
		bsr read_controller
		move.b d3,d7
		or.b #$7f,d7
		cmpi.b #$7f,d7
		beq endtitle
		
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe,d7
		beq cursorup	

		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd,d7
		beq cursordown
		
		move.b d3,d7
		or.b #$ef,d7
		cmpi.b #$ef,d7
		beq additem
		
		move.b d3,d7
		or.b #$bf,d7
		cmpi.b #$bf,d7
		beq subitem
		
		bra titleloop
		
subitem:
		cmpi.w #$ab4a,cursorpos
		beq subheight
		cmpi.w #$abca,cursorpos
		beq substreamers
		cmpi.w #$ac4a,cursorpos
		beq subspeed
		bra titleloop
		
subheight:
		sub.w #$01, height
		move.l #$6b440002,(a3)
		move.w height, d1
		andi.w #$f0,d1
		lsr.w #$04,d1
		add.w #$41b0,d1
		move.w d1,(a4)
		move.w height, d1
		andi.w #$0f,d1
		add.w #$41b0,d1
		move.w d1,(a4)
		bsr unhold
		bra titleloop
		
substreamers:
		sub.w #$01, streamers
		move.l #$6bc40002,(a3)
		move.w streamers, d1
		andi.w #$f0,d1
		lsr.w #$04,d1
		add.w #$41b0,d1
		move.w d1,(a4)
		move.w streamers, d1
		andi.w #$0f,d1
		add.w #$41b0,d1
		move.w d1,(a4)
		bsr unhold
		bra titleloop
		
subspeed:
		sub.w #$01, speed
		move.l #$6c440002,(a3)
		move.w speed, d1
		andi.w #$f0,d1
		lsr.w #$04,d1
		add.w #$41b0,d1
		move.w d1,(a4)
		move.w speed, d1
		andi.w #$0f,d1
		add.w #$41b0,d1
		move.w d1,(a4)
		bsr unhold
		bra titleloop
	
additem:
		cmpi.w #$ab4a,cursorpos
		beq addheight
		cmpi.w #$abca,cursorpos
		beq addstreamers
		cmpi.w #$ac4a,cursorpos
		beq addspeed
		bra titleloop
		
addheight:
		add.w #$01, height
		move.l #$6b440002,(a3)
		move.w height, d1
		andi.w #$f0,d1
		lsr.w #$04,d1
		add.w #$41b0,d1
		move.w d1,(a4)
		move.w height, d1
		andi.w #$0f,d1
		add.w #$41b0,d1
		move.w d1,(a4)
		bsr unhold
		bra titleloop
		
addstreamers:
		add.w #$01, streamers
		move.l #$6bc40002,(a3)
		move.w streamers, d1
		andi.w #$f0,d1
		lsr.w #$04,d1
		add.w #$41b0,d1
		move.w d1,(a4)
		move.w streamers, d1
		andi.w #$0f,d1
		add.w #$41b0,d1
		move.w d1,(a4)
		bsr unhold
		bra titleloop
		
addspeed:
		add.w #$01, speed
		move.l #$6c440002,(a3)
		move.w speed, d1
		andi.w #$f0,d1
		lsr.w #$04,d1
		add.w #$41b0,d1
		move.w d1,(a4)
		move.w speed, d1
		andi.w #$0f,d1
		add.w #$41b0,d1
		move.w d1,(a4)
		bsr unhold
		bra titleloop
		
cursorup:
		cmpi.w #$ab4a,cursorpos
		ble titleloop	
		moveq #$00000000,d0
		move.w cursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)		
		moveq #$00000000,d0
		sub.w #$0080,cursorpos
		move.w cursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$413c,(a4)
		move.w #$412d,(a4)
		move.w #$412d,(a4)
		bsr unhold
		bra titleloop
cursordown:
		cmpi.w #$ac4a,cursorpos
		bge titleloop	
		moveq #$00000000,d0
		move.w cursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)				
		moveq #$00000000,d0
		add.w #$0080,cursorpos
		move.w cursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$413c,(a4)
		move.w #$412d,(a4)
		move.w #$412d,(a4)
		bsr unhold
		bra titleloop		
endtitle:
		rts