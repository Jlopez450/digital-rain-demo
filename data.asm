palette:
	dc.w $0000, $0CEC, $0000, $0000, $0000, $0000, $0000, $0000
	dc.w $0000, $0000, $0000, $0000, $0000, $0000, $0000, $0000	;near white	
	dc.w $0000, $06E6, $0000, $0000, $0000, $0000, $0000, $0000
	dc.w $0000, $0000, $0000, $0000, $0000, $0000, $0000, $0000 ;light green	
	dc.w $0000, $00E0, $0000, $0000, $0000, $0000, $0000, $0000
	dc.w $0000, $0000, $0000, $0000, $0000, $0000, $0000, $0000 ;bright green	
	dc.w $0000, $0060, $0000, $0000, $0000, $0000, $0000, $0000
	dc.w $0000, $0000, $0000, $0000, $0000, $0000, $0000, $0000 ;dark green
	
VDPSetupArray:
	dc.w $8014		
	dc.w $8174  ;Genesis mode, DMA enabled, VBLANK-INT enabled		
	dc.w $8228	;field A    
	dc.w $8300	;$833e	
	dc.w $8405	;field B	
	dc.w $8518	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8Aff		
	dc.w $8B00		
	dc.w $8C81	
	dc.w $8D34		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200
	
title:
	dc.b "                                        "
	dc.b "  Matrix digital rain demo for Genesis  "
	dc.b "                                        "
	dc.b "          Press START to begin          "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "  Programmed in 68K assembly language.  "
	dc.b "  Build date: Jan/05/2015               "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "                                        "
	dc.b "  Animation settings:                   "
	dc.b "  Press A to subtract, B to add.        "
	dc.b "  Alter at your own risk.               "
	dc.b "                                        "
	dc.b "  Minimum starting height        -18+<--"
	dc.b "  # of streamers (>48 may lag)   -30+   "
	dc.b "  Animate speed (lower is faster)-03+   "
	dc.b "                                        "
	dc.b "  Visit www.mode5.net (c)2016 ComradeOj "
	dc.b "      Contact: ComradeOj@yahoo.com     %"
	
font:
	incbin "gfx/combo.bin"
	
ascii:
	incbin "gfx/ascii.bin"
	incbin "gfx/hexascii.bin"