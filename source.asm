    include "ramdat.asm"
    include "header.asm"
start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.w #$2700, sr       ;disable ints
		bsr clear_regs
		bsr clear_ram
        bsr setup_vdp
		bsr clear_vram
		lea (font),a5
		move.w #$0BDF,d4
		move.l #$40000000,(a3)
		bsr vram_Loop
		move.l #$c0000000,(a3)
		lea (palette),a5
		move.w #$003f,d4
		bsr vram_loop
		bsr titlescreen
		
		move.l #$40000010,(a3)		;vsram
		move.w #$0000,(a4)
		move.w #$0004,(a4)
		move.l #$50000003,(a3)		;hsram
		move.w #$0000,(a4)
		move.w #$0084,(a4)

		move.w #$9B00,d3
		move.w #$9B00,threads
		move.w #$0001,tile
        move.w #$2300, sr       ;enable ints		
		
loop:
		bsr randomtile
		bsr randompos
		bsr randomtile
		bsr randomstart
		;bsr randomreplace2
		bsr randomreplace
		bsr randomreplace2
		bra loop
				
randomreplace:
		add.w #$01, replacement
		cmpi.w #$BF,replacement
		bge replacerestart
		rts
replacerestart:
		move.w #$0001,replacement
		rts
		
randomreplace2:
		add.w #$03, replacement2
		cmpi.w #$BF,replacement2
		bge replacerestart2
		rts
replacerestart2:
		move.w #$0001,replacement2
		rts
		
randomstart:
		add.w #$01, randstart
		move.w height,d2
		cmp.w randstart,d2
		; cmpi.w #$18,randstart
		; bge resetstart
		ble resetstart
		rts
resetstart:
		move.w #$0000,randstart
		rts
		
randompos:
		add.w #$02,starting
		cmpi.w #$50,starting
		bge resetpos
		rts
resetpos:
		move.w #$00,starting
		rts
		
randomtile:
		add.w #$01,tile
		cmpi.w #$00A8,tile
		bge resettile
		rts
resettile:
		move.w #$0000,tile
		bra randomtile

setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts  
vram_Loop:
        move.w (a5)+,(a4)
        dbf d4,vram_Loop
        rts  
clear_regs:
		moveq #$00000000,d0
		move.l d0,d1
		move.l d0,d2
		move.l d0,d3
		move.l d0,d4
		move.l d0,d5
		move.l d0,d6
		move.l d0,d7
		move.l d0,a0
		move.l d0,a1
		move.l d0,a2
		move.l d0,a3
		move.l d0,a4
		move.l d0,a5
		move.l d0,a6
		rts
		
clear_ram:
		move.w #$7FF0,d0
		move.l #$FF0000,a0	
clear_ram_loop:
		move.w #$0000,(a0)+
		dbf d0, clear_ram_loop
		rts
clear_vram:
		move.l #$40000000,(a3)
		move.w #$7fff,d4
vram_clear_loop:
		move.w #$0000,(a4)
		dbf d4, vram_clear_loop
		rts		
		
calc_vram:
		;move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		;move.l (sp)+,d1
		rts	
calc_vram_read:
		;move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		;eor.w #$0000,d0  ;attach vram read bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		;move.l (sp)+,d1
		rts	
		
ErrorTrap:        
        bra ErrorTrap
		
returnint:
		rte
return:
		rts

HBlank:
        rte
		

VBlank:
		add.b #$01,inverter
		move.w speed,d5
		cmp.b inverter,d5	;speed
		bne returnint
		move.b #$00,inverter	
		lea (threads),a5
		move.w streamers,d4		;number of streamers
		
animateloop:
		bsr randompos
		move.w (a5),d3
		bsr drop
		move.w d3,(a5)+
		bsr randomstart
		dbf d4, animateloop
		bsr alter_letter
        rte
	
alter_letter:
		lea (font),a5
		move.w replacement,d7
		lsl.w #$05,d7
		add.w d7,a5		
		move.l #$0000,d0
		move.w replacement2,d7
		lsl.w #$05,d7
		
		add.w d7,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w (a5)+,(a4)
		move.w (a5)+,(a4)
		move.w (a5)+,(a4)
		move.w (a5)+,(a4)
		move.w (a5)+,(a4)
		move.w (a5)+,(a4)
		move.w (a5)+,(a4)
		move.w (a5)+,(a4)
		move.w (a5)+,(a4)
		move.w (a5)+,(a4)
		move.w (a5)+,(a4)
		move.w (a5)+,(a4)
		move.w (a5)+,(a4)
		move.w (a5)+,(a4)
		move.w (a5)+,(a4)
		move.w (a5)+,(a4)
		rts
	
drop:
		cmpi.w #$B380,d3	;was AE00. Can now cut out slow color reset code
		bge resetdrop	
		move.l d3,d0
		bsr calc_vram
		move.l d0,(a3)
		add.w #$01,tile
		add.w #$80,d3
		move.w tile,d7
		move.w d7,(a4)
		
		;bsr color		
		;rts		
color:	
		sub.w #$580,d3
		
		move.l d3,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0000,(a4)		
		add.w #$200,d3
		
		move.l d3,d0
		bsr calc_vram_read
		move.l d0,(a3)
		move.w (a4),d6
		andi.w #$00FF,d6
		eor.w #$6000,d6
		move.l d3,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w d6,(a4)

		add.w #$180,d3
		move.l d3,d0
		bsr calc_vram_read
		move.l d0,(a3)
		move.w (a4),d6
		andi.w #$00FF,d6
		eor.w #$4000,d6
		move.l d3,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w d6,(a4)	

		add.w #$100,d3
		move.l d3,d0
		bsr calc_vram_read
		move.l d0,(a3)
		move.w (a4),d6
		andi.w #$00FF,d6
		eor.w #$2000,d6
		move.l d3,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w d6,(a4)	

		add.w #$80,d3
		move.l d3,d0
		bsr calc_vram_read
		move.l d0,(a3)
		move.w (a4),d6
		andi.w #$00FF,d6
		;eor.w #$6000,d6
		move.l d3,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w d6,(a4)
		add.w #$80,d3
		rts

resetdrop:	
		move.w #$9B00,d3
		add.w starting,d3
		move.w randstart,d2
		lsl.w #$07,d2
		add.w d2,d3
		bra drop
		
read_controller:                ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		rts	
		
unhold:
		bsr read_controller
		cmpi.b #$ff,d3
		 beq return
		bra unhold
		
termtextloop:
		move.w #$0027,d4		;draw 40 cells of text
		clr d5	
textloop:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return		
		andi.w #$00ff,d5
		add.l #$4100,d5
        move.w d5,(a4)	
		dbf d4,textloop	
		move.w #$0017,d4	    ;draw 24 cells of nothing
emptyloop:		
		move.w #$0000,(a4)
		dbf d4,emptyloop
		bra termtextloop		
		
	include "titlescreen.asm"
	include "data.asm"
	
	dc.b "Nothing left to see, scroll up already! "

ROM_End:            